<div style="padding-bottom: 3px; height: 7%">
    <span>ID:</span>
    <input id="product_id" style="line-height:26px;border:1px solid #ccc">
    <span>Category:</span>
    <input id="product_category" style="line-height:26px;border:1px solid #ccc">
    <span>Code:</span>
    <input id="product_code" style="line-height:26px;border:1px solid #ccc">
    <span>Product Name:</span>
    <input id="product_name" style="line-height:26px;border:1px solid #ccc">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="product_doSearch()">Search</a>
</div>
<table id="product_dg" class="easyui-datagrid" title="Products"
		data-options="
			iconCls: 'icon-edit',
			singleSelect: true,
			toolbar: '#product_tb',
			url: '<?= site_url('product/datas'); ?>',
			method: 'get',
		" pagination="true" style="height: 92%">
	<thead>
		<tr>
			<th data-options="field:'id',width:80,sortable:true">Product ID</th>
			<th data-options="field:'category0',width:150,editor:'textbox',sortable:true">Category</th>
			<th data-options="field:'code',width:100,editor:'textbox',sortable:true">Code</th>
			<th data-options="field:'name',width:200,editor:'textbox',sortable:true">Product Name</th>
			<th data-options="field:'description',width:250,editor:'textbox'">Description</th>
			<th field="22" formatter="product_img_format" data-options="width:200">Image</th>
			<th field="23" formatter="product_sort_format">Action</th>
		</tr>
	</thead>
</table>

<div id="product_tb" style="height:auto">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="product_create()">Create Product</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onclick="product_reload()">Reload</a>
</div>

<script type="text/javascript">
	function product_reload(){
		$('#product_dg').datagrid('reload');
		// alert('Reloaded');
	}

	function product_sort_format(val,row,index){ 
		return "<div><a href='javascript:;' onclick='product_edit("+row.id+")'>update</a> | <a href='javascript:;' onclick='product_hapus("+row.id+")'>delete</a> </div>";
	} 
	
	function product_img_format(val,row,index){
		if(row.image===null)
			return 'no image';
		else
			return '<img src="<?= base_url() ?>uploads/'+row.image+'" width="150">';
	} 

	function product_hapus(ids){
		if(confirm('are you sure?'))
			$.get( "<?= site_url('product/delete'); ?>/"+ids )
			  .done(function(data) {
			    alert(data.msg);
			    $('#product_dg').datagrid('reload');
			  });
	}

	function product_edit(ids){
		$("#product_dlg_content").attr("src", '<?= site_url('product/update') ?>/'+ids);
		$('#product_dlg').dialog('open');
	}
	
	function product_create(){
		$("#product_dlg_content").attr("src", '<?= site_url('product/create') ?>/');
		$('#product_dlg').dialog('open');
	}
	function isExist(url){
	    var http = new XMLHttpRequest();
	    http.open('HEAD', "<?= base_url() ?>uploads/"+url, false);
	    http.send();
	    return http.status!=404;
	}

	function product_doSearch(){
        $('#product_dg').datagrid('load',{
            params: {id : $('#product_id').val(), category0 : $('#product_category').val(), code : $('#product_code').val(), name : $('#product_name').val() },
        });
    }

</script>
<div id="product_dlg" class="easyui-dialog" closed="true" title="Product" data-options="iconCls:'icon-save'" style="width:800px;height:400px;padding:0px;overflow-y: hidden;overflow-x: hidden;">
	<iframe id="product_dlg_content" style="width:100%; height: 100%"></iframe>
</div>
