<h2><?= ($this->MProduct->id==null)? "Create":"update" ?> Category</h2>
	<p>Fill the form and submit it.</p>
	<div style="margin:20px 0;"></div>
	<div class="easyui-panel" title="Category" style="width:100%;padding:30px 60px;">
		<form id="ff" enctype="multipart/form-data" method="post" action="<?= current_url(); ?>">
			<div style="margin-bottom:20px">
				<input id="category" class="easyui-combobox" name="category" style="width:100%;" data-options="
						url:'<?= site_url('category/datasForCombo'); ?>',
						method:'get',
						valueField:'id',
						textField:'name',
						panelHeight:'auto',
						label: 'Category:',
						labelPosition: 'left',
						required:true
						">
			</div>

			<div style="margin-bottom:20px">
				<input value="<?= $this->MProduct->name ?>" class="easyui-textbox" name="name" style="width:100%" data-options="label:'Name:',required:true">
			</div>
			<div style="margin-bottom:20px">
				<input value="<?= $this->MProduct->code ?>" class="easyui-textbox" name="code" style="width:100%" data-options="label:'Code:',required:true">
			</div>
			<div style="margin-bottom:20px">
				<input value="<?= $this->MProduct->description ?>" class="easyui-textbox" name="description" style="width:100%" style="width:100%;height:60px" data-options="label:'Description:',multiline:true">
			</div>
			<div style="margin-bottom:20px">
				<input id="image" name="image" class="easyui-filebox" style="width:100%" style="width:100%;height:60px" data-options="label:'Image:',prompt:'Choose a file...',buttonAlign:'right'">
			</div>
			<div style="text-align:center;padding:5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" data-options="iconCls:'icon-save'">Submit</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" data-options="iconCls:'icon-remove'">Clear</a>
			</div>
		</form>

	</div>
	<script>
		function submitForm(){
			$('#ff').trigger("submit");
		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>
	<script type="text/javascript">
		$(function(){
		  $('#ff').submit(function(){
		  	if(!$(this).form('enableValidation').form('validate'))
		  		return false;

		    // $.post($(this).attr('action'), $(this).serialize(), function(json) {
		    //   	alert(json.msg);
		    //   	window.parent.$('#product_dlg').dialog('close');
		    //   	parent.product_reload();
		    // }, 'json');
		    // return false;
		    $('#ff').form({
		        url:'<?= current_url(); ?>',
		        ajax:'true',
		        iframe:'false', // pour activer le onProgress
		        success: function(json){
		        	json = JSON.parse(json);
		        	alert(json.msg);
	                if(json.stat=='OK'){
				      	window.parent.$('#product_dlg').dialog('close');
				      	parent.product_reload();
		                }
		        },
		    });
		    return false;
		  });

		 $('#category').combobox({
				onLoadSuccess: function(param){
					$('#category').combobox('setValue',<?= $this->MProduct->category ?>);
				}
			});
		});
	</script>