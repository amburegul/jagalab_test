<h2><?= ($this->MCategory->id==null)? "Create":"update" ?> Category</h2>
	<p>Fill the form and submit it.</p>
	<div style="margin:20px 0;"></div>
	<div class="easyui-panel" title="Category" style="width:100%;padding:30px 60px;">
		<form id="ff" method="post" action="<?= current_url(); ?>">
			<div style="margin-bottom:20px">
				<input value="<?= $this->MCategory->name ?>" class="easyui-textbox" name="name" style="width:100%" data-options="label:'Category:',required:true">
			</div>
			<div style="text-align:center;padding:5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" data-options="iconCls:'icon-save'">Submit</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" data-options="iconCls:'icon-remove'">Clear</a>
			</div>
		</form>

	</div>
	<script type="text/javascript">
		$(function(){
		  $('#ff').submit(function(){
		  	if(!$(this).form('enableValidation').form('validate'))
		  		return false;

		    $.post($(this).attr('action'), $(this).serialize(), function(json) {
		      	alert(json.msg);
		      	window.parent.$('#category_dlg').dialog('close');
		      	parent.category_reload();
		    }, 'json');
		    return false;
		  });
		});
	</script>
	<script>
		function submitForm(){
			$('#ff').trigger( "submit" );
		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>
