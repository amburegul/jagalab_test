<div style="padding-bottom: 3px; height: 7%">
    <span>ID:</span>
    <input id="category_id" style="line-height:26px;border:1px solid #ccc">
    <span>Category:</span>
    <input id="category_name" style="line-height:26px;border:1px solid #ccc">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="category_doSearch()">Search</a>
</div>
<table id="category_dg" class="easyui-datagrid" title="Categories" style="height: 92%"
		data-options="
			iconCls: 'icon-edit',
			singleSelect: true,
			toolbar: '#category_tb',
			url: '<?= site_url('category/datas'); ?>',
			method: 'get',
		" pagination="true">
	<thead>
		<tr>
			<th data-options="field:'id',width:80,sortable:true">Category ID</th>
			<th data-options="field:'name',width:250,editor:'textbox',sortable:true">Category</th>
			<th field="23" formatter="category_sort_format">Action</th>
		</tr>
	</thead>
</table>

<div id="category_tb" style="height:auto">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="category_create()">Create Category</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onclick="category_reload()">Reload</a>
</div>

<script type="text/javascript">
	function category_reload(){
		$('#category_dg').datagrid('reload');
		// alert('Reloaded');
	}

	function category_sort_format(val,row,index){ 
		return "<div><a href='javascript:;'  onclick='category_edit("+row.id+")'>update</a> | <a href='javascript:;' onclick='category_hapus("+row.id+")'>delete</a> </div>";
	} 
	function category_hapus(ids){
		if(confirm('are you sure?'))
			$.get( "<?= site_url('category/delete'); ?>/"+ids )
			  .done(function(data) {
			    alert(data.msg);
			    $('#category_dg').datagrid('reload');
			  });
	}

	function category_edit(ids){
		$("#category_dlg_content").attr("src", '<?= site_url('category/update') ?>/'+ids);
		$('#category_dlg').dialog('open');
	}
	
	function category_create(){
		$("#category_dlg_content").attr("src", '<?= site_url('category/create') ?>/');
		$('#category_dlg').dialog('open');
	}

	function category_doSearch(){
        $('#category_dg').datagrid('load',{
            params: {id : $('#category_id').val(), name : $('#category_name').val() },
        });
    }

</script>
<div id="category_dlg" class="easyui-dialog" closed="true" title="Category" data-options="iconCls:'icon-save'" style="width:800px;height:400px;padding:0px;overflow-y: hidden;overflow-x: hidden;">
	<iframe id="category_dlg_content" style="width:100%; height: 100%"></iframe>
</div>
