<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jagalab Test.</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/demo/demo.css">
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-eeasyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-eeasyui/jquery.easyui.min.js"></script>
</head>

<body class="easyui-layout">
	<div data-options="region:'center',title:'Selamat Datang...',iconCls:'icon-ok'">
		<div class="easyui-tabs" data-options="fit:true,border:false,plain:true">
			<div title="Category" data-options="href:'<?= site_url('category/index') ?>'" style="padding:10px">
			</div>
			<div title="Product" style="padding:10px" data-options="href:'<?= site_url('product/index') ?>'">

			</div>
		</div>
	</div>
</body>
</html>