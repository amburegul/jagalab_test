<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/js/jquery-eeasyui/demo/demo.css">
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-eeasyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-eeasyui/jquery.easyui.min.js"></script>
</head>

<body>
	
	<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
	<!-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

	<?php $this->load->view($content); ?>
</body>

</html>