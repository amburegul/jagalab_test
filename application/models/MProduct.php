<?php
class MProduct extends CI_Model {
        private $tb_name = 'product';

        public $id;
        public $category;
        public $code;
        public $name;
        public $description;
        public $image;

        public function countAllRows(){
            $query = $this->db
                ->select('count(*) as jlh')
                ->get($this->tb_name);
            return $query->row_array()['jlh'];
        }

        public function convertToEasyUIFormat($datas){
                $json['total'] = $this->countAllRows();
                $json['rows']= array_values($datas);
                return json_encode($json);
        }

        public function get_datas($params,$offset,$limit)
        {
                $query = $this->db
                    ->limit($limit, $offset)
                    ->like('A.id',$params['id'])
                    ->like('A.name',$params['name'])
                    ->like('code',$params['code'])
                    ->like('B.name',$params['category0'])
                    ->join('category B', 'A.category =B.id')->select('A.*, B.name as category0')->get($this->tb_name.' A');
                return $query->result_array();
        }

        public function delete($id)
        {
                $this->db->where('id', $id);
                return $this->db->delete($this->tb_name);
        }

        public function findOne($id)
        {
                $query = $this->db->where('id',$id)->get($this->tb_name);
                $this->id = $query->row('id');
                $this->category = $query->row('category');
                $this->code = $query->row('code');
                $this->name = $query->row('name');
                $this->description = $query->row('description');
                $this->image = $query->row('image');
        }

        public function save() {
                if($this->id==null)
                        return $this->db->insert($this->tb_name, $this);
                else
                        return $this->db->update($this->tb_name, $this, array('id' => $this->id));
         }

}