<?php
class MCategory extends CI_Model {
        private $tb_name = 'category';

        public $id;
        public $name;

        public function countAllRows(){
            $query = $this->db
                ->select('count(*) as jlh')
                ->get($this->tb_name);
            return $query->row_array()['jlh'];
        }

        public function convertToEasyUIFormat($datas){
                $json['total'] = $this->countAllRows();
                $json['rows']= array_values($datas);
                return json_encode($json);
        }
        public function convertToEasyUICombo($datas){
                $json = array_values($datas);
                return json_encode($json);
        }

        public function get_datas($params,$offset,$limit)
        {
                $query = $this->db
                    ->limit($limit, $offset)
                    ->like('id', $params['id'])
                    ->like('name', $params['name'])
                    ->get($this->tb_name);
                return $query->result_array();
        }

        public function delete($id)
        {
                $this->db->where('id', $id);
                return $this->db->delete($this->tb_name);
        }

        public function findOne($id)
        {
                $query = $this->db->where('id',$id)->get($this->tb_name);
                $this->id = $query->row('id');
                $this->name = $query->row('name');
        }

        public function save() {
                if($this->id==null)
                        return $this->db->insert($this->tb_name, $this);
                else
                        return $this->db->update($this->tb_name, $this, array('id' => $this->id));
         }

}