<?php
class Category extends CI_Controller {
		public $template = 'layouts/ajax';
		public function jsonHeader(){
			header('Content-Type: application/json');
		}
        public function index()
        {
            return $this->load->view($this->template,
            		[
            			'content'=>'category/index',
            		]
            	);
        }

        public function datas(){
            $page = ($this->input->get('page')!='') ? intval($this->input->get('page')) : 1;
            $rows = ($this->input->get('rows')!='') ? intval($this->input->get('rows')) : 10;
            $offset = ($page-1)*$rows;

            $params = $this->input->get('params');
            $this->load->model('MCategory');
            $this->jsonHeader();
            die($this->MCategory->convertToEasyUIFormat($this->MCategory->get_datas($params,$offset,$rows)));
        }

        public function datasForCombo(){
        	$this->load->model('MCategory');
        	$this->jsonHeader();
        	die($this->MCategory->convertToEasyUICombo($this->MCategory->get_datas()));
        }

        public function delete($id){
        	$this->load->model('MCategory');
        	$this->jsonHeader();
        	if($this->MCategory->delete($id) === FALSE)
        		die(json_encode(['stat'=>'KO','msg'=>'Delete Falied!']));
        	else
        		die(json_encode(['stat'=>'OK','msg'=>'Delete Success!']));
        }

        public function update($id){
        	$this->load->model('MCategory');
        	$model = $this->MCategory->findOne($id);
        	$this->load->helper(array('form', 'url'));

            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Category', 'required');
			$this->form_validation->run();

        	if ($this->form_validation->run() == FALSE)
            {
                return $this->load->view($this->template,
            		[
            			'content'=>'category/_form',
            		]
            	);
            }
            else
            {
                $this->MCategory->name = $this->input->post('name');
                if($this->MCategory->save())
                {
                	$this->jsonHeader();
                	die(json_encode(['stat'=>'OK','msg'=> 'Update Success!']));
                }
            }
        	
        }

        public function create(){
        	$this->load->model('MCategory');
        	$this->load->helper(array('form', 'url'));

            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Category', 'required');
			$this->form_validation->run();

        	if ($this->form_validation->run() == FALSE)
            {
                return $this->load->view($this->template,
            		[
            			'content'=>'category/_form',
            		]
            	);
            }
            else
            {
                $this->MCategory->name = $this->input->post('name');
                if($this->MCategory->save())
                {
                	$this->jsonHeader();
                	die(json_encode(['stat'=>'OK','msg'=> 'Save Success!']));
                }
            }
        	
        }


}