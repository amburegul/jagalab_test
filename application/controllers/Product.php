<?php
class Product extends CI_Controller {
		public $template = 'layouts/ajax';
		public function jsonHeader(){
			header('Content-Type: application/json');
		}
        public function index()
        {
            return $this->load->view($this->template,
            		[
            			'content'=>'product/index',
            		]
            	);
        }

        public function datas(){
            $page = ($this->input->get('page')!='') ? intval($this->input->get('page')) : 1;
            $rows = ($this->input->get('rows')!='') ? intval($this->input->get('rows')) : 10;
            $offset = ($page-1)*$rows;
            
            $params = $this->input->get('params');
        	$this->load->model('MProduct');
        	$this->jsonHeader();
        	die($this->MProduct->convertToEasyUIFormat($this->MProduct->get_datas($params,$offset,$rows)));
        }

        public function delete($id){
        	$this->load->model('MProduct');
        	$this->jsonHeader();
            $this->MProduct->findOne($id);
            $model = $this->MProduct;

        	if($this->MProduct->delete($id) === FALSE)
        		die(json_encode(['stat'=>'KO','msg'=>'Delete Falied!']));
        	else{
                @unlink(FCPATH."uploads/".$model->image);
        		die(json_encode(['stat'=>'OK','msg'=>'Delete Success!']));
            }
        }

        public function update($id){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['encrypt_name'] = TRUE;

            $this->load->model('MProduct');
            $model = $this->MProduct->findOne($id);

            $this->load->helper(array('form', 'url'));
            $this->load->library('upload', $config);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('code', 'Code', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->run();
        	if ($this->form_validation->run() == FALSE)
            {
                return $this->load->view($this->template,
            		[
            			'content'=>'product/_form',
            		]
            	);
            }
            else
            {
                $this->MProduct->description = $this->input->post('description');
                $this->MProduct->category = $this->input->post('category');
                $this->MProduct->code = $this->input->post('code');
                $this->MProduct->name = $this->input->post('name');
    
                if (!empty($_FILES['image']['name'])) {
                    if(!$this->upload->do_upload('image')){
                        $this->jsonHeader();
                        die(json_encode(['stat'=>'KO','msg'=> strip_tags($this->upload->display_errors())]));
                    }
                    elseif($result = $this->upload->data()){
                        $result = $this->upload->data();
                        $this->MProduct->image = $result['file_name'];
                        @unlink(FCPATH."uploads/".$model->image);
                    }
                }

                if($this->MProduct->save()){
                    $this->jsonHeader();
                    die(json_encode(['stat'=>'OK','msg'=> 'Save Success!']));
                }
            }
        	
        }

        public function create(){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['encrypt_name'] = TRUE;

            $this->load->model('MProduct');

            $this->load->helper(array('form', 'url'));
            $this->load->library('upload', $config);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('code', 'Code', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->run();

            if ($this->form_validation->run() == FALSE)
            {
                return $this->load->view($this->template,
                    [
                        'content'=>'product/_form',
                    ]
                );
            }
            else
            {
                $this->MProduct->description = $this->input->post('description');
                $this->MProduct->category = $this->input->post('category');
                $this->MProduct->code = $this->input->post('code');
                $this->MProduct->name = $this->input->post('name');
                if (!empty($_FILES['image']['name'])) {
                    if(!$this->upload->do_upload('image')){
                        $this->jsonHeader();
                        die(json_encode(['stat'=>'KO','msg'=> strip_tags($this->upload->display_errors())]));
                    }
                    elseif($result = $this->upload->data()){
                        $result = $this->upload->data();
                        $this->MProduct->image = $result['file_name'];
                    }
                }

                if($this->MProduct->save()){
                    $this->jsonHeader();
                    die(json_encode(['stat'=>'OK','msg'=> 'Save Success!']));
                }
            }
            
        }

}